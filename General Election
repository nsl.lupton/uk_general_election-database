CREATE DATABASE General_Election;

USE General_Election;

CREATE TABLE ge(
  ons_id VARCHAR(10),
  ons_region_id VARCHAR(10),
  constituency_name VARCHAR(50),
  county_name VARCHAR(50),
  region_name VARCHAR(50),
  country_name VARCHAR(50),
  constituency_type VARCHAR(10),
  party_name VARCHAR(50),
  party_abbreviation VARCHAR(50),
  firstname VARCHAR(50),
  surname VARCHAR(50),
  gender VARCHAR(6),
  sitting_mp VARCHAR(3),
  former_mp VARCHAR(3),
  votes INT,
  share FLOAT,
  change VARCHAR(20),
  PRIMARY KEY(ons_id,firstname,surname));

//import data from cvs file in table ge

CREATE TABLE party(
 party_id VARCHAR(50) PRIMARY KEY,
 party_name VARCHAR(50));

CREATE TABLE region(
 ons_region_id VARCHAR(10) PRIMARY KEY,
 region_name VARCHAR(50),
 country_name VARCHAR(50));

CREATE TABLE county(
 county_name VARCHAR(50) PRIMARY KEY,
 ons_region_id VARCHAR(10) NOT NULL,
 FOREIGN KEY (ons_region_id) REFERENCES region(ons_region_id));

CREATE TABLE constituency(
 ons_id VARCHAR(10) PRIMARY KEY,
 constituency VARCHAR(50) NOT NULL UNIQUE,
 county_name VARCHAR(50) NOT NULL,
 constituency_type VARCHAR(10)
 NOT NULL
 CHECK (constituency_type IN ('county','borough')),
 FOREIGN KEY (county_name) REFERENCES county(county_name));

CREATE TABLE candidate(
 ons_id VARCHAR(10),
 firstname VARCHAR(50),
 surname VARCHAR(50),
 gender VARCHAR(10) NOT NULL,
 party_id VARCHAR(50) NULL,
 sitting_mp VARCHAR(3) NOT NULL CHECK (sitting_mp IN ('Yes','No')),
 former_mp VARCHAR(3) NOT NULL CHECK (former_mp IN ('Yes','No')),
 votes INT NOT NULL,
 share FLOAT NOT NULL,
 `change` FLOAT NULL,
 PRIMARY KEY (ons_id,firstname,surname),
 FOREIGN KEY (ons_id) REFERENCES constituency(ons_id),
 FOREIGN KEY (party_id) REFERENCES party(party_id));

INSERT INTO region
 SELECT DISTINCT ons_region_id, region_name, country_name
 FROM ge;

INSERT INTO party
 SELECT party_abbreviation, MIN(party_name)
 FROM ge
 GROUP BY party_abbreviation;

INSERT INTO county
 SELECT DISTINCT county_name,ons_region_id
 FROM ge;

INSERT INTO constituency
 SELECT DISTINCT ons_id, constituency_name, county_name, constituency_type
 FROM ge;

INSERT INTO candidate
 SELECT ons_id, firstname, surname, gender, NULLIF(party_abbreviation,'Ind'),
 sitting_mp,former_mp,votes,share,NULLIF(`change`,'')
 FROM ge;

DROP TABLE ge;